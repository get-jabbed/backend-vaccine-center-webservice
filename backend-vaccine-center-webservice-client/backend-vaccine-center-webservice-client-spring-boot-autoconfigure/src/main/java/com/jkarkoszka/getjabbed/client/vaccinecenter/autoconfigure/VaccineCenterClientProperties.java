package com.jkarkoszka.getjabbed.client.vaccinecenter.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * Class with the webservice client configuration.
 */
@ConfigurationProperties(prefix = "getjabbed.vaccine-center-client")
@Data
@Validated
public class VaccineCenterClientProperties {

  private String url;
}
