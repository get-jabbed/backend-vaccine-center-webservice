package com.jkarkoszka.getjabbed.client.vaccinecenter.autoconfigure;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterWebserviceClient;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Autoconfiguration for web client (with logbook).
 */
@Configuration
@EnableConfigurationProperties(VaccineCenterClientProperties.class)
@AllArgsConstructor
public class VaccineCenterClientAutoConfiguration {

  @Bean
  @ConditionalOnProperty("getjabbed.vaccine-center-client.url")
  VaccineCenterWebserviceClient vaccineCenterWebserviceClient(
      WebClient webClient,
      VaccineCenterClientProperties vaccineCenterClientProperties) {
    return new VaccineCenterWebserviceClient(webClient, vaccineCenterClientProperties);
  }
}
