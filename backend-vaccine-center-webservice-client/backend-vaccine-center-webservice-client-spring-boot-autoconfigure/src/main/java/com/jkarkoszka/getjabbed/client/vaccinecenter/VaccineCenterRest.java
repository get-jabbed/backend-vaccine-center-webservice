package com.jkarkoszka.getjabbed.client.vaccinecenter;

import lombok.Builder;
import lombok.Value;

/**
 * Vaccine center REST model.
 */
@Value
@Builder
public class VaccineCenterRest {

  String id;
  String name;
}
