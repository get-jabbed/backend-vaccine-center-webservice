package com.jkarkoszka.getjabbed.client.vaccinecenter;

import com.jkarkoszka.getjabbed.client.vaccinecenter.autoconfigure.VaccineCenterClientProperties;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Vaccine center webservice client.
 */
@AllArgsConstructor
public class VaccineCenterWebserviceClient {

  private final WebClient webClient;
  private final VaccineCenterClientProperties vaccineCenterClientProperties;

  /**
   * Method to find vaccine center by registerToken.
   *
   * @param authenticationToken authentication token
   * @param registerToken       register token
   * @return vaccine center
   */
  public Mono<VaccineCenterRest> findByRegisterToken(String authenticationToken,
                                                     String registerToken) {
    return webClient.method(HttpMethod.GET)
        .uri(vaccineCenterClientProperties.getUrl()
            + "/vaccine-center?registerToken={registerToken}", registerToken)
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + authenticationToken)
        .retrieve()
        .bodyToMono(VaccineCenterRest.class);
  }
}
