package com.jkarkoszka.getjabbed.client.vaccinecenter.mock;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import lombok.SneakyThrows;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Delay;
import org.mockserver.model.Header;
import org.springframework.stereotype.Component;

@Component
public class VaccineCenterWebserviceMock {

  private int PORT = 21455;

  private ClientAndServer clientAndServer;
  private ObjectMapper objectMapper;

  public VaccineCenterWebserviceMock(ObjectMapper objectMapper) {
    clientAndServer = startClientAndServer(PORT);
    this.objectMapper = objectMapper;
  }

  public void reset() {
    clientAndServer.reset();
  }

  @SneakyThrows
  public void findVaccineCenterMock(String registerToken,
                                    VaccineCenterRest vaccineCenterRest,
                                    int status,
                                    int exactlyTimes) {
    var body = objectMapper.writeValueAsString(vaccineCenterRest);
    new MockServerClient("localhost", PORT)
        .when(
            request()
                .withMethod("GET")
                .withPath(String.format("/vaccine-center"))
                .withQueryStringParameter("registerToken", registerToken),
            exactly(exactlyTimes))
        .respond(
            response()
                .withDelay(Delay.seconds(0))
                .withStatusCode(status)
                .withHeaders(new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Keep-Alive", "max, unknown=test, timeout=10"))
                .withBody(body));
  }
}
