package com.jkarkoszka.getjabbed.client.vaccinecenter;

import static org.assertj.core.api.Assertions.assertThat;

import com.jkarkoszka.getjabbed.client.vaccinecenter.mock.VaccineCenterWebserviceMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import testproject.TestApplication;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = {
    TestApplication.class, VaccineCenterWebserviceMock.class})
public class VaccineCenterWebserviceClientTest {

  @Autowired
  VaccineCenterWebserviceClient vaccineCenterWebserviceClient;

  @Autowired
  VaccineCenterWebserviceMock vaccineCenterWebserviceMock;

  @BeforeEach
  void setUp() {
    vaccineCenterWebserviceMock.reset();
  }


  @Test
  void shouldReturnVaccineCenterByRegisterToken() {
    //given
    var backendServiceToken = "abc";
    var registerToken = "secret_key";
    var id = "123";
    var name = "name123";
    var expectedVaccineCenterRest = VaccineCenterRest.builder()
        .id(id)
        .name(name)
        .build();
    vaccineCenterWebserviceMock.findVaccineCenterMock(registerToken, expectedVaccineCenterRest,
        200, 1);

    //when
    var vaccineCenterRest = vaccineCenterWebserviceClient
        .findByRegisterToken(backendServiceToken, registerToken).block();

    //then
    assertThat(vaccineCenterRest).isEqualTo(expectedVaccineCenterRest);
  }
}
