FROM adoptopenjdk/openjdk11:alpine-jre
COPY backend-vaccine-center-webservice-impl/target/*.jar /app.jar
ENTRYPOINT java $JAVA_OPTS -jar "/app.jar"