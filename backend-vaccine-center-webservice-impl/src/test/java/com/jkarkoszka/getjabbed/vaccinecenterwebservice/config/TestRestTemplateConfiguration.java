package com.jkarkoszka.getjabbed.vaccinecenterwebservice.config;

import com.jkarkoszka.getjabbed.security.autoconfigure.SecurityProperties;
import com.jkarkoszka.getjabbed.security.service.JwtTokenGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TestRestTemplateConfiguration {

  @Value("${server.port}")
  private int port;

  @Autowired
  JwtTokenGeneratorService jwtTokenGeneratorService;

  @Autowired
  SecurityProperties securityProperties;

  @Bean
  public RestTemplate testRestTemplateAsBackendService() {
    return new RestTemplateBuilder()
        .rootUri("http://localhost:" + port)
        .defaultHeader("Authorization", "Bearer " + securityProperties.getBackendServiceToken())
        .build();
  }
}
