package com.jkarkoszka.getjabbed.vaccinecenterwebservice.findvaccinecenter.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.client.VaccineCenterWebserviceTestClient;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDb;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDbRepository;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class FindVaccineCenterControllerTest {

  @Autowired
  VaccineCenterWebserviceTestClient vaccineCenterWebserviceTestClient;

  @Autowired
  RestTemplate testRestTemplate;

  @Autowired
  VaccineCenterDbRepository vaccineCenterDbRepository;

  @Test
  void shouldReturnVaccineCenterIfRegisterTokenIsCorrect() {
    //given
    var id = new ObjectId();
    var idAsString = id.toHexString();
    var name = "Center 1";
    var registerToken = "abc123";
    var vaccineCenterDb = VaccineCenterDb.builder()
        .id(id)
        .name(name)
        .registerToken(registerToken)
        .createdAt(LocalDateTime.now(ZoneOffset.UTC))
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
    vaccineCenterDbRepository.save(vaccineCenterDb).block();
    var expectedVaccineCenterRest = VaccineCenterRest.builder()
        .id(idAsString)
        .name(name)
        .build();

    //when
    var response = vaccineCenterWebserviceTestClient.findVaccineCenter(registerToken);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(expectedVaccineCenterRest);
  }

  @Test
  void shouldReturn404IfRegisterTokenIsNotCorrect() {
    //given
    var registerToken = "abc123";

    //when//then
    try {
      vaccineCenterWebserviceTestClient.findVaccineCenter(registerToken);
      fail("expected exception");
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
  }

  @Test
  void shouldReturn401IfNoAccess() {
    //given
    var registerToken = "abc123";

    //when//then
    try {
      vaccineCenterWebserviceTestClient.findVaccineCenter(testRestTemplate, registerToken);
      fail("expected exception");
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
  }
}
