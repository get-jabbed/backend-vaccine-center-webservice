package com.jkarkoszka.getjabbed.vaccinecenterwebservice.client;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.web.CreateVaccineCenterRest;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class VaccineCenterWebserviceTestClient {

  @Autowired
  private final RestTemplate testRestTemplateAsBackendService;

  public ResponseEntity<VaccineCenterRest> findVaccineCenter(RestTemplate restTemplate,
                                                             String registerToken) {
    return restTemplate.getForEntity(
        "/vaccine-center?registerToken=" + registerToken, VaccineCenterRest.class);
  }

  public ResponseEntity<VaccineCenterRest> findVaccineCenter(String registerToken) {
    return findVaccineCenter(testRestTemplateAsBackendService, registerToken);
  }

  public ResponseEntity<VaccineCenterRest> createVaccineCenter(
      RestTemplate restTemplate, CreateVaccineCenterRest createVaccineCenterRest) {
    return restTemplate.postForEntity(
        "/vaccine-center", createVaccineCenterRest, VaccineCenterRest.class);
  }

  public ResponseEntity<VaccineCenterRest> createVaccineCenter(
      CreateVaccineCenterRest createVaccineCenterRest) {
    return createVaccineCenter(testRestTemplateAsBackendService, createVaccineCenterRest);
  }
}
