package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import com.jkarkoszka.getjabbed.vaccinecenterwebservice.client.VaccineCenterWebserviceTestClient;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDb;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDbRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class CreateVaccineCenterControllerTest {

  @Autowired
  VaccineCenterWebserviceTestClient vaccineCenterWebserviceTestClient;

  @Autowired
  VaccineCenterDbRepository vaccineCenterDbRepository;

  @Autowired
  RestTemplate testRestTemplate;

  @BeforeEach
  void setUp() {
    vaccineCenterDbRepository.deleteAll();
  }

  @Test
  void shouldCreateVaccineCenter() {
    //given
    String name = "Center2";
    String registerToken = "abc";
    var createVaccineCenterRest = CreateVaccineCenterRest.builder()
        .name(name)
        .registerToken(registerToken)
        .build();
    var expectedVaccineCenterDb = VaccineCenterDb.builder()
        .name(name)
        .registerToken(registerToken)
        .build();

    //when
    var response = vaccineCenterWebserviceTestClient.createVaccineCenter(createVaccineCenterRest);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody().getId()).isNotBlank();
    assertThat(response.getBody().getName()).isEqualTo(name);
    var vaccineCenterDb = vaccineCenterDbRepository.findByRegisterToken(registerToken).block();
    assertThat(vaccineCenterDb)
        .usingRecursiveComparison()
        .ignoringFields("id", "createdAt", "updatedAt")
        .isEqualTo(expectedVaccineCenterDb);
  }

  @Test
  void shouldReturn401IfNoAccess() {
    //given
    String name = "Center2";
    String registerToken = "abc";
    var createVaccineCenterRest = CreateVaccineCenterRest.builder()
        .name(name)
        .registerToken(registerToken)
        .build();

    //when//then
    try {
      vaccineCenterWebserviceTestClient.createVaccineCenter(testRestTemplate, createVaccineCenterRest);
      fail("expected exception");
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
  }
}