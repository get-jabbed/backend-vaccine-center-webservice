package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service;

import com.jkarkoszka.getjabbed.db.mapper.ObjectIdMapper;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDb;
import org.mapstruct.Mapper;

/**
 * VaccineCenter/VaccineCenterDb mapper.
 */
@Mapper(uses = ObjectIdMapper.class)
public interface VaccineCenterMapper {

  VaccineCenter map(VaccineCenterDb vaccineCenterDb);
}
