package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service;

import java.time.LocalDateTime;
import lombok.Value;

/**
 * Vaccine center service model.
 */
@Value
public class VaccineCenter {

  String id;
  String name;
  String registerToken;
  LocalDateTime updatedAt;
  LocalDateTime createdAt;
}
