package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception on vaccine center not found.
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class VaccineCenterNotFoundException extends RuntimeException {

  public VaccineCenterNotFoundException(String registerToken) {
    super(String.format("Vaccine center with register token = '%s' not found.", registerToken));
  }
}
