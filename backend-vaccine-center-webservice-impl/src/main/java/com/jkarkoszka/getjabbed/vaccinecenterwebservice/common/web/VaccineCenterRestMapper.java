package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.web;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service.VaccineCenter;
import org.mapstruct.Mapper;

/**
 * VaccineCenterRest/VaccineCenter mapper.
 */
@Mapper
public interface VaccineCenterRestMapper {

  VaccineCenterRest map(VaccineCenter vaccineCenter);
}
