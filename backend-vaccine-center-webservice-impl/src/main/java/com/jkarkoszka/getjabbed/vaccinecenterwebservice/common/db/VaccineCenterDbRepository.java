package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

/**
 * Vaccine center repository.
 */
public interface VaccineCenterDbRepository
    extends ReactiveMongoRepository<VaccineCenterDb, ObjectId> {

  Mono<VaccineCenterDb> findByRegisterToken(String registerToken);
}
