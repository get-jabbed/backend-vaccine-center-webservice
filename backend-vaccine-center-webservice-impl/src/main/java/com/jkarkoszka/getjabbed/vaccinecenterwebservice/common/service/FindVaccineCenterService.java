package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service;

import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDbRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Service to find vaccine center.
 */
@Component
@AllArgsConstructor
public class FindVaccineCenterService {

  private final VaccineCenterDbRepository vaccineCenterDbRepository;
  private final VaccineCenterMapper vaccineCenterMapper;

  /**
   * Method to get vaccine center by register token.
   *
   * @param registerToken register token
   * @return vaccine center
   */
  public Mono<VaccineCenter> findByRegisterToken(String registerToken) {
    return vaccineCenterDbRepository.findByRegisterToken(registerToken)
        .map(vaccineCenterMapper::map)
        .switchIfEmpty(Mono.error(new VaccineCenterNotFoundException(registerToken)));
  }
}
