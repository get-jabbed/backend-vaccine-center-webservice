package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.web;

import com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.service.CreateVaccineCenter;
import org.mapstruct.Mapper;

/**
 * CreateVaccineCenter/CreateVaccineCenterRest mapper.
 */
@Mapper
public interface CreateVaccineCenterMapper {

  CreateVaccineCenter map(CreateVaccineCenterRest createVaccineCenterRest);
}
