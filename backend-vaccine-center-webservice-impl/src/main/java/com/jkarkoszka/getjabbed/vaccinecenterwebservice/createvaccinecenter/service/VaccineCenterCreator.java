package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.service;

import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDb;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.bson.types.ObjectId;

class VaccineCenterCreator {

  public static VaccineCenterDb create(CreateVaccineCenter createVaccineCenter) {
    return VaccineCenterDb.builder()
        .id(new ObjectId())
        .name(createVaccineCenter.getName())
        .registerToken(createVaccineCenter.getRegisterToken())
        .createdAt(LocalDateTime.now(ZoneOffset.UTC))
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
