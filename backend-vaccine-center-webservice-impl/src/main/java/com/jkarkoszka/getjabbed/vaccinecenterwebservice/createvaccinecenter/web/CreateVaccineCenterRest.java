package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.web;

import lombok.Builder;
import lombok.Value;

/**
 * Create vaccine center rest request.
 */
@Value
@Builder
public class CreateVaccineCenterRest {

  String name;
  String registerToken;
}
