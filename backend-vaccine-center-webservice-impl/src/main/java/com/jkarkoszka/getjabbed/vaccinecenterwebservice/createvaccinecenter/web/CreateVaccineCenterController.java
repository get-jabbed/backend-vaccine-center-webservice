package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.web;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.web.VaccineCenterRestMapper;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.service.CreateVaccineCenterService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Endpoints to create vaccine center.
 */
@RestController
@AllArgsConstructor
public class CreateVaccineCenterController {

  private CreateVaccineCenterService createVaccineCenterService;
  private CreateVaccineCenterMapper createVaccineCenterMapper;
  private VaccineCenterRestMapper vaccineCenterRestMapper;

  /**
   * Endpoint to create vaccine center.
   *
   * @param createVaccineCenterRest create vaccine center rest
   * @return vaccine center
   */
  @PostMapping("/vaccine-center")
  public Mono<VaccineCenterRest> create(
      @RequestBody CreateVaccineCenterRest createVaccineCenterRest) {
    return Mono.fromSupplier(() -> createVaccineCenterMapper.map(createVaccineCenterRest))
        .flatMap(createVaccineCenterService::create)
        .map(vaccineCenterRestMapper::map);
  }
}
