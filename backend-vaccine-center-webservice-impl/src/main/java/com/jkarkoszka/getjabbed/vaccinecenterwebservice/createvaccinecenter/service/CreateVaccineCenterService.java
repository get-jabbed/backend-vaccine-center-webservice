package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.service;

import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db.VaccineCenterDbRepository;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service.VaccineCenter;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service.VaccineCenterMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Service to create vaccine center.
 */
@Component
@AllArgsConstructor
public class CreateVaccineCenterService {

  private final VaccineCenterDbRepository vaccineCenterDbRepository;
  private final VaccineCenterMapper vaccineCenterMapper;

  /**
   * Method to create vaccine center.
   *
   * @param createVaccineCenter create vaccine center request
   * @return vaccine center
   */
  public Mono<VaccineCenter> create(CreateVaccineCenter createVaccineCenter) {
    return Mono.fromSupplier(() -> VaccineCenterCreator.create(createVaccineCenter))
        .flatMap(vaccineCenterDbRepository::save)
        .map(vaccineCenterMapper::map);
  }
}
