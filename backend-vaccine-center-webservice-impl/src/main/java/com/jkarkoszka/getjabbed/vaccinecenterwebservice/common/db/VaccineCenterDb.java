package com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.db;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 * Vaccine center db model.
 */
@Data
@Builder
public class VaccineCenterDb {

  @Id
  private ObjectId id;
  private String name;
  private String registerToken;
  private LocalDateTime updatedAt;
  private LocalDateTime createdAt;
}
