package com.jkarkoszka.getjabbed.vaccinecenterwebservice.findvaccinecenter.web;

import com.jkarkoszka.getjabbed.client.vaccinecenter.VaccineCenterRest;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.service.FindVaccineCenterService;
import com.jkarkoszka.getjabbed.vaccinecenterwebservice.common.web.VaccineCenterRestMapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Endpoints to find vaccine center.
 */
@RestController
@AllArgsConstructor
public class FindVaccineCenterController {

  private FindVaccineCenterService findVaccineCenterService;
  private VaccineCenterRestMapper vaccineCenterRestMapper;

  @GetMapping("/vaccine-center")
  public Mono<VaccineCenterRest> findByRegisterToken(
      @RequestParam("registerToken") String registerToken) {
    return findVaccineCenterService.findByRegisterToken(registerToken)
        .map(vaccineCenterRestMapper::map);
  }
}
