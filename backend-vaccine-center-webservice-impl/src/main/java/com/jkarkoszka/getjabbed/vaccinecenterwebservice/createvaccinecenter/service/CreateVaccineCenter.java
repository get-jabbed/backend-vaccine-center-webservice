package com.jkarkoszka.getjabbed.vaccinecenterwebservice.createvaccinecenter.service;

import lombok.Value;

/**
 * Create vaccine center service request.
 */
@Value
public class CreateVaccineCenter {

  String name;
  String registerToken;
}
